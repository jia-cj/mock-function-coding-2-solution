let collection = {};

let frontIndex = 0;
let rearIndex = -1;

function print() {
  const printedElements = [];
  for (let i = frontIndex; i <= rearIndex; i++) {
    printedElements[printedElements.length] = collection[i];
  }
  return printedElements;
}


function enqueue(element) {
  rearIndex++;
  collection[rearIndex] = element;
  return print();
}


function dequeue() {
  if (isEmpty()) {
    console.log("Queue is empty");
    return undefined;
  }
  const removedItem = collection[frontIndex];
  delete collection[frontIndex];
  frontIndex++;
  return removedItem;
}



function front() {
  if (isEmpty()) {
    console.log("Queue is empty");
    return undefined; 
  }
  return collection[frontIndex];
}

function size() {
  return rearIndex - frontIndex + 1;
}

function isEmpty() {
  return size() === 0;
}

module.exports = {
  collection,
  print,
  enqueue,
  dequeue,
  front,
  size,
  isEmpty,
};
